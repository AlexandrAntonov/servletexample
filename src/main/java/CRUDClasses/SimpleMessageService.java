package CRUDClasses;

import entity.SimpleMessage;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

//Классический CRUD-объект, управление состоянием транзакции вынесено в методы
public class SimpleMessageService {
    public EntityManager em = Persistence.createEntityManagerFactory("persist").createEntityManager();

    //Завернем методы работы с транзакцией
    public void beginTransaction(){
        em.getTransaction().begin();
    }
    public void commitTransaction(){
        em.getTransaction().commit();
    }
    public void rollbackTransaction(){
        em.getTransaction().rollback();
    }


    public SimpleMessage add(SimpleMessage simpleMessage){
        SimpleMessage smFromDB = em.merge(simpleMessage);
        return smFromDB;
    }

    public void delete(long id){
        em.remove(get(id));
    }

    public void update(SimpleMessage simpleMessage){
        em.merge(simpleMessage);
    }

    public SimpleMessage get(long id){
        return em.find(SimpleMessage.class, id);
    }

    public List<SimpleMessage> getAll(){
        TypedQuery<SimpleMessage> namedQuery = em.createNamedQuery("SimpleMessage.getAll", SimpleMessage.class);
        return namedQuery.getResultList();
    }
}
