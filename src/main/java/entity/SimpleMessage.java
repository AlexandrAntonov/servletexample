package entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "simplemessage")
@NamedQuery(name = "SimpleMessage.getAll", query = "SELECT mes FROM SimpleMessage mes ORDER BY id DESC")
public class SimpleMessage {

    //Генерацию id взвалим на хрупкие плечи JPA механизмов
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    //Текст некоего сообщения, которое будет добавлено в БД
    @Column(name = "message", length = 255)
    private String message;

    //дата добавления сообщения
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    public SimpleMessage(String message, Date createDate){
        this.message = message;
        this.createDate = createDate;
    }

    public SimpleMessage() {
    }

    public long getId(){
        return id;
    }

    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return message;
    }

    public void setCreateDate(Date createDate){
        this.createDate = createDate;
    }
    public Date getCreateDate(){
        return createDate;
    }
}
