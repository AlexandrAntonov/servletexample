import CRUDClasses.SimpleMessageService;
import entity.SimpleMessage;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

//маппинг УРЛа на веб-сервере
@WebServlet(urlPatterns = "/", name = "testServlet")
public class ServletExample extends HttpServlet {
    public SimpleMessageService smServ = new SimpleMessageService();

    protected String checkMessageText(String textToCheck){
        //проверки на отсутствие поля или его пустоту
        if( textToCheck == null || textToCheck.isEmpty()){
            return "Message text is empty!";
        }
        //проверим ограничение длинны поля сообщения
        if(textToCheck.length() > 254){
            return  "Message text too long!";
        }
        return null;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        //подсовываем в диспетчер запросов нашу подготовленную страничку
        List<SimpleMessage> smList = smServ.getAll();
        req.setCharacterEncoding("UTF-8"); //обозначим что параметры запроса будут обрабатыватся в кодировке UTF-8, аналогично и в ПОСТ
        req.setAttribute("messageList", smList );
        req.getRequestDispatcher("start.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        //почистим поля для ошибок
        req.setAttribute("addError", "");
        req.setAttribute("editError", "");
        req.setAttribute("delError", "");
        //посмотрим что было нажато
        String submit = req.getParameter("submit");

        switch (submit){
            case "Add":
                String simpleMessage = req.getParameter("mesTxt");
                String addErr = checkMessageText(simpleMessage);
                if( addErr != null ){
                    req.setAttribute("addError", addErr );
                    break;
                }
                smServ.beginTransaction();
                    smServ.add(new SimpleMessage(simpleMessage, new Date()));
                smServ.commitTransaction();
                break;
            case "Edit":
                String editId = req.getParameter("idEdit");
                if(editId.isEmpty()){
                    req.setAttribute("editError", "No message is selected" );
                    break;
                }
                String editMessage = req.getParameter("mesEdit");

                String editErr = checkMessageText(editMessage);
                if ( editErr != null ){
                    req.setAttribute("editError", editErr );
                    break;
                }

                Long idToEdit;
                try {
                    idToEdit = Long.parseLong(editId);
                }catch (NumberFormatException e){
                    req.setAttribute("editError", "Message id is wrong!");
                    break;
                }
                SimpleMessage messToEdit = smServ.get(idToEdit);
                if(messToEdit == null){
                    req.setAttribute("editError", "Message with id="+editId+" not found!");
                    break;
                }
                messToEdit.setMessage(editMessage);
                smServ.beginTransaction();
                    smServ.update(messToEdit);
                smServ.commitTransaction();
                break;
            case "Delete":
                String stringId = req.getParameter("delId");
                //Немного проверок на примитивные ошибки
                if(stringId.isEmpty()){
                    req.setAttribute("delError", "Message id is empty!" );
                    break;
                }
                HashSet<Long> ids = new HashSet<>();
                String[] splitIds = stringId.split(";");//разберем полученый список id на массив и перегоним в ХэшСет, чтоб не было повторений
                for(String simpleId : splitIds){
                    Long id;
                    try {
                        id = Long.parseLong(simpleId);
                    }catch (NumberFormatException e){
                        req.setAttribute("delError", "Message id is wrong!");
                        break;
                    }
                    ids.add(id);
                }
                if(ids.size() > 0) {
                    smServ.beginTransaction();//откроем транзакцию и начнем заливать список id для удаления. если в
                                              //списках есть неизвестные id соответственно откатится весь набор удалений
                        for (Long delId : ids) {
                            try {
                                smServ.delete(delId);
                            } catch (IllegalStateException | IllegalArgumentException e) {
                                req.setAttribute("delError", "Delete error!");
                                smServ.rollbackTransaction();
                                break;
                            }
                        }
                    smServ.commitTransaction();
                }
                break;
        }
        doGet(req, resp);
    }
}
