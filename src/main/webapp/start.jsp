<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--Простенькая страничка для сервлета, отображает содержимое таблицы в БД, сортировка по порядку добавления, по убыванию--%>
<html>
<head>
  <title>ServletExample</title>
</head>
<body>
<style type="text/css">/*немного стилей для общей красоты*/
    table {
        border-collapse: collapse;
    }
    td{
        padding: 3px;
        border: 1px solid black;
    }

    thead td{
        background: lightcyan
    }

    tbody tr:hover{
        background: lightcoral;
    }/*класс псевдо-кнопки на основе ячейки таблицы*/
    .button {
        border: 1px solid white;
        width: 50px;
        background: goldenrod;
        padding-left: 5px;
        cursor: pointer;
    }
    .dsbld{ /*классик имитирует дисейбл поля ввода вкупе со свойством ридонли. если провесить просто дисейбл - jsp не передает
              именованное поле в качестве параметра запроса, потому возник такой небольшой хак*/
        background: #eaeaea;
    }
</style>
<script>
    //в обе функции приходит родительский элемент ячейки, т.е. <TR>, в котором уже по ячейкам слобцов извлекаются данные
    function editFunc(element){
        for (var j = 0, col; col = element.cells[j], j <2; j++) {
            var attr = col.getAttribute("name");
            if(attr === "id")
                document.getElementById("idEdit").value = col.innerText;
            if(attr === "message")
                document.getElementById("mesEdit").value = col.innerText;
        }
    }
    function delFunc(element){
        var col = element.cells[0];
        //собираем значения id с троку с разделителем ";" для группового удаления
        document.getElementById("idDel").value = document.getElementById("idDel").value.concat(col.innerText).concat(";");

    }
</script>
<form action="testServlet" name="controlForm" method="POST">
<div>
    <p>Enter new message text:<br>
        <input type="text" name="mesTxt"><br>
        <input type="submit" name="submit" value="Add"><br>
        <div style="color: red">${addError}</div>
    </p>
</div>
<div>
    <p>Redact the message text:<br>
        <input type="text" name="idEdit" id="idEdit" readonly class="dsbld"><br>
        <input type="text" name="mesEdit" id="mesEdit"><br>
        <input type="submit" name="submit" value="Edit"><br>
    <div style="color: red">${editError}</div>
    </p>
</div>
<div>
    <p>Enter message id to delete:<br>
        <input name="delId" id="idDel" readonly class="dsbld"><br>
        <input type="submit" name="submit" value="Delete"><br>
        <div style="color: red">${delError}</div>
    </p>
</div>
</form>
<table id="messages">
    <thead>
        <tr>
            <td style="width: 50px">id</td>
            <td style="width: 150px">message text</td>
            <td style="width: 250px">add date</td>
            <td class="button" style="background: white; cursor: default">&nbsp;</td>
            <td class="button" style="background: white; cursor: default">&nbsp;</td>
        </tr>
    </thead>
    <tbody>
    <c:forEach var="message" items="${messageList}">
        <tr>
            <td name="id"><c:out value="${message.id}"/></td>
            <td name="message"><c:out value="${message.message}"/></td>
            <td name="createDate"><c:out value="${message.createDate}"/></td>
            <td onclick="editFunc(this.parentNode)" class="button">Edit</td>
            <td onclick="delFunc(this.parentNode)" class="button">Delete</td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>