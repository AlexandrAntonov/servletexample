import CRUDClasses.SimpleMessageService;
import entity.SimpleMessage;
import org.junit.Test;
import static junit.framework.Assert.*;

import java.util.Date;
import java.util.List;

public class SimpleMessageServiceTest {
    SimpleMessageService smServ = new SimpleMessageService();

    //Создаем месседж в БД
    @Test
    public void testSaveRecord() throws Exception {
        SimpleMessage sm1 = new SimpleMessage();
        sm1.setMessage("First message");
        sm1.setCreateDate(new Date());
        smServ.beginTransaction();
        SimpleMessage sm = smServ.add(sm1);
        smServ.commitTransaction();
        System.out.println(sm.getMessage());
        assertNotNull("Тест сохранения, проверяем id", sm.getId());

        //почистим запись из бд
        smServ.beginTransaction();
        smServ.delete(sm.getId());
        smServ.commitTransaction();
    }

    //Создадим и удалим месседж из БД
    @Test
    public void testDeleteRecord() throws Exception {
        SimpleMessage sm1 = new SimpleMessage();
        sm1.setMessage("Message for delete");
        sm1.setCreateDate(new Date());
        smServ.beginTransaction();
        SimpleMessage sm = smServ.add(sm1);
        smServ.commitTransaction();
        assertNotNull("Тест удаления, проверяем id", sm.getId());
        smServ.beginTransaction();
        smServ.delete(sm.getId());
        smServ.commitTransaction();
    }

    //Создадим и модифицируем запись в БД
    @Test
    public void testUpdate() throws Exception {
        SimpleMessage sm1 = new SimpleMessage();
        sm1.setMessage("Message for delete");
        sm1.setCreateDate(new Date());
        smServ.beginTransaction();
        sm1 = smServ.add(sm1);
        smServ.commitTransaction();
        sm1.setMessage("Message was updated");
        smServ.beginTransaction();
        smServ.update(sm1);
        smServ.commitTransaction();

        assertNotNull("Тест модификации, проверяем id созданой записи", sm1.getId());

        //Получаем обновленую запись
        SimpleMessage sm = smServ.get(sm1.getId());
        System.out.println(sm);
        assertEquals("Message was updated", sm.getMessage());

        //почистим запись из бд
        smServ.beginTransaction();
        smServ.delete(sm.getId());
        smServ.commitTransaction();
    }

    //Тестим метод запроса всех записей
    @Test
    public void testGetAll(){
        SimpleMessage sm1 = new SimpleMessage();
        sm1.setMessage("First test message");
        sm1.setCreateDate(new Date());
        SimpleMessage sm2 = new SimpleMessage();
        sm2.setMessage("Second test message");
        sm2.setCreateDate(new Date());
        SimpleMessage sm3 = new SimpleMessage();
        sm3.setMessage("Third test message");
        sm3.setCreateDate(new Date());

        smServ.beginTransaction();
        long id1 = smServ.add(sm1).getId();
        long id2 = smServ.add(sm2).getId();
        long id3 = smServ.add(sm3).getId();
        smServ.commitTransaction();

        List<SimpleMessage> sMess = smServ.getAll();

        assertNotNull("Тест получения записей, проверяем есть ли список", sMess);
        assertTrue("Тест получения записей, убедимся что список не пуст", sMess.size() > 0);

        for(SimpleMessage sm : sMess){
            System.out.println(sm);
        }

        //Почистим созданые записи
        smServ.beginTransaction();
        smServ.delete(id1);
        smServ.delete(id2);
        smServ.delete(id3);
        smServ.commitTransaction();
    }
}
